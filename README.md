# Gnome-builder-phpactor

A Language Server Protocol PHP plugin for 
[Gnome-Builder](https://wiki.gnome.org/Apps/Builder) using 
[Phpactor](https://github.com/phpactor/phpactor) as backend.

## Installation 

```bash
meson setup --prefix $HOME/.local -Dphpactor_cmd="$HOME/phpactor/bin/phpactor" build
ninja -C build
ninja -C build install
```

This will install the plugin in your `$HOME/.locale/share/gnome-builder/plugins` directory.
The option `-Dphpactor_cmd` is used to find the phpactor executable where you installed it.

See [phpactor installation](https://phpactor.readthedocs.io/en/master/usage/standalone.html)
